The Child Theme works on the following process:-

Architecture:-
Due to how the AutoIT Plugin is set up, we can modify its templates without touching it by using a child theme. For the sending and recieving of Ajax requests we use Jquery to disable the former ones and add ours in place of the default ones.
Initial Setup:-

We will setup a weekly cron job that will make a request to the Autoit Web API to pull the following information for every vehicle:-

Type of Gear
Number of Doors
Car Registration
 Mileage

Then this will be saved in the database as an associate array with the keys as the document_id. Each Vehicle has a Unique Document ID for example this Audi Etron has the ID AD4119689.

We also save information like the types of gears and the number of doors so we can use them as options in our Search Shortcode.

This helps in making repeated requests to the API as the requests made for all vehicles by the plugin doesn’t provide us with specific information we need. We already have this specific information for each vehicle in our database and it gets updated every week or every day, at a time it looks like there are around 202 vehicles provided by the API.

This API Requests function will most likely be inside the functions.php of a child theme.


You can check the Schema and results provided by the API when all vehicles are requested:-
https://drive.google.com/file/d/1ja7Q_8cNGli1Dy7aX3wvUM5pHvROr0XL/view?usp=share_link

You can check the Schema and results provided by the API when a single vehicle is requested:- https://drive.google.com/file/d/1k_amjKfoF9o4gVbYM-Hp7uAlhRlbzpif/view?usp=sharing


Modifying the Search Shortcode:-

Here we modify the Search Shortcode to add our own options, so user can use them for filtering, 
The options will be the following:-

• Filter for the type of gear (Dropdown options)
• Filter for the number of doors (Drop down options)
• Filter for when the car was registered (Range slider)
• Filter for how many kilometers that car has driven (Range slider)


Modifying the Search Results Shortcode:-

Here we modify the results that have been retrieved originally via the API, now we use our saved data(from the weekly API requests) and use it to further remove the vehicles that don't match our criteria. So let’s say the API Originally Retrieved 50 Results, but only 10 of them fit right criteria(The user’s selected Gears, Number of Doors, Car Registration Date, Mileage, etc) then our code will filter out the non-matched 40 Results will forward the matching 10 results to be displayed in front of the user.  
