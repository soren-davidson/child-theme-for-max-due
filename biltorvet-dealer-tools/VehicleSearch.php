<?php
	/**
	 * Vehicle search (search parameters and filters) template.
	 *
	 * This template can be overriden by copying this file to your-theme/biltorvet-dealer-tools/VehicleSearch.php
	 *
	 * @author      Biltorvet A/S
	 * @package     Biltorvet Dealer Tools
	 * @version     1.0.0
	 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

	global $wp;
	$root          = dirname( dirname( dirname( plugin_dir_url( __FILE__ ) ) ) );
	$currentPage   = get_query_var( 'bdt_page', -1 );
	$bdt_root_url  = home_url( $wp->request );
	$filterOptions = get_option( 'filter_options' );

if ( $currentPage === -1 ) {
	$currentPage = 1;
} else {
	$bdt_root_url = rtrim( $bdt_root_url, '/' );
}

	// The initial filter is only here to hide dropdowns with one value only, so they don't "flash" before Ajax executes.
	// All filter logic should be in JS.
try {
	$filterObject = new BDTFilterObject();

	if ( isset( $this->_options_2['hide_sold_vehicles'] ) && $this->_options_2['hide_sold_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideSoldVehicles = 'true';
	}
	if ( isset( $this->_options_2['hide_leasing_vehicles'] ) && $this->_options_2['hide_leasing_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideLeasingVehicles = 'true';
	}
	if ( isset( $this->_options_2['hide_flexleasing_vehicles'] ) && $this->_options_2['hide_flexleasing_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideFlexLeasingVehicles = 'true';
	}
	if ( isset( $this->_options_2['hide_warehousesale_vehicles'] ) && $this->_options_2['hide_warehousesale_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideWarehousesaleVehicles = 'true';
	}
	if ( isset( $this->_options_2['hide_carlite_dealer_label_vehicles'] ) && $this->_options_2['hide_carlite_dealer_label_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideCarLiteDealerLabelVehicles = 'true';
	}
	if ( isset( $this->_options_2['hide_export_vehicles'] ) && $this->_options_2['hide_export_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideExportVehicles = 'true';
	}
	if ( isset( $this->_options_2['hide_upcoming_vehicles'] ) && $this->_options_2['hide_upcoming_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideUpcomingVehicles = 'true';
	}
	if ( isset( $this->_options_2['hide_rental_vehicles'] ) && $this->_options_2['hide_rental_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideRentalVehicles = 'true';
	}
	if ( isset( $this->_options_2['hide_commission_vehicle'] ) && $this->_options_2['hide_commission_vehicle'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideCommissionVehicles = 'true';
	}
	if ( isset( $this->_options_2['hide_wholesale_vehicle'] ) && $this->_options_2['hide_wholesale_vehicle'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideWholesaleVehicles = 'true';
	}
	if ( isset( $this->_options_2['hide_trailer_vehicle'] ) && $this->_options_2['hide_trailer_vehicle'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideTrailerVehicles = 'true';
	}
	if ( isset( $this->_options_2['hide_typecar_vehicles'] ) && $this->_options_2['hide_typecar_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideByTypeCar = 'true';
	}
	if ( isset( $this->_options_2['hide_typevan_vehicles'] ) && $this->_options_2['hide_typevan_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideByTypeVan = 'true';
	}
	if ( isset( $this->_options_2['hide_typemotorcycle_vehicles'] ) && $this->_options_2['hide_typemotorcycle_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideByTypeMotorcycle = 'true';
	}
	if ( isset( $this->_options_2['hide_typetruck_vehicles'] ) && $this->_options_2['hide_typetruck_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideByTypeTruck = 'true';
	}
	if ( isset( $this->_options_2['hide_typebus_vehicles'] ) && $this->_options_2['hide_typebus_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideByTypeBus = 'true';
	}
	if ( isset( $this->_options_2['hide_brandnew_vehicles'] ) && $this->_options_2['hide_brandnew_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideBrandNewVehicles = 'true';
	}
	if ( isset( $this->_options['hide_ad_vehicles'] ) && $this->_options['hide_ad_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideADVehicles = 'true';
	}
	if ( isset( $this->_options['hide_bi_vehicles'] ) && $this->_options['hide_bi_vehicles'] === 'on' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->HideBIVehicles = 'true';
	}
	if ( isset( $this->_options_2['bdt_pricetypes'] ) && $this->_options_2['bdt_pricetypes'] != '-1' ) {
		if ( $filterObject === null ) {
			$filterObject = new BDTFilterObject();
		}
		$filterObject->PriceTypes = array( $this->_options_2['bdt_pricetypes'] );
	}

	$initialFilterOptions = $this->biltorvetAPI->GetFilterOptions( $filterObject );
} catch ( Exception $e ) {
	die( $e->getMessage() );
}

	$showCompanies     = ( count( (array) $initialFilterOptions->companies ) > 1 ) ? '' : "style='display: none;'";
	$showVehicleStates = ( count( (array) $initialFilterOptions->vehicleStates ) >= 2 ) ? '' : "style='display: none;'";
	$showMakeModel     = ( count( $initialFilterOptions->makes ) >= 1 ) ? '' : "style='display: none;'";
	$showProductTypes  = ( count( (array) $initialFilterOptions->companies ) > 2 || count( $initialFilterOptions->productTypes ) > 1 ) ? '' : "style='display: none;'";

	$showPriceTypes = '';
if ( $initialFilterOptions->priceTypes != null && ( count( (array) $initialFilterOptions->priceTypes ) > 1 ) ) {
	if ( isset( $this->_options_2['bdt_pricetypes'] ) && $this->_options_2['bdt_pricetypes'] === '-1' ) {
		$showPriceTypes = '';
	} elseif ( isset( $this->_options_2['bdt_pricetypes'] ) && $this->_options_2['bdt_pricetypes'] !== '-1' ) {
		$showPriceTypes = "style='display: none;'";
	} else {
		$showPriceTypes = '';
	}
} else {
	$showPriceTypes = "style='display: none;'";
}

	$showBodyTypes   = ( count( (array) $initialFilterOptions->companies ) > 1 || count( $initialFilterOptions->bodyTypes ) > 1 ) ? '' : "style='display: none;'";
	$showPropellants = ( count( (array) $initialFilterOptions->companies ) > 1 || count( $initialFilterOptions->propellants ) > 1 ) ? '' : "style='display: none;'";

	// Custom Vehicle Types - variables only related to the frontpage search
	$showCustomVehicleTypesSection = ( isset( $this->_options_2 ) ) && ( isset( $this->_options_2['vehiclesearch_activate_iconbased_search'] ) ) && ( $this->_options_2['vehiclesearch_activate_iconbased_search'] === 'on' ) ? '' : 'display: none;';

	// Custom color selected?
	$customVehicleTypeIconColor = ( isset( $this->_options_2 ) ) && ( isset( $this->_options_2['frontpagesearch_iconbased_search_icon_color'] ) ) && $this->_options_2['frontpagesearch_iconbased_search_icon_color'] !== '' ? $this->_options_2['frontpagesearch_iconbased_search_icon_color'] : ( isset( $this->_options['primary_color'] ) && trim( $this->_options['primary_color'] ) !== '' ? $this->_options['primary_color'] : '#00a1b7' );

?>

	<div class="bdt">
		<div class="vehicle_search">
			<span class="hide-bdt animate__animated animate__fadeIn" id="bdt-loading-filters">
				<div class="car-model-container" style="<?php echo $showCustomVehicleTypesSection; ?>">
					<?php

						require Biltorvet::bdt_get_template( '/partials/_customVehicleTypes.php' );

					?>
				</div>
			<div class="row">

				<?php if ( isset( $this->_options_2['fulltextsearch_or_quicksearch'] ) && $this->_options_2['fulltextsearch_or_quicksearch'] === '1' ) : ?>
					<div class="col-sm-12 mb-3 mb-sm-3" id="quicksearch-input">
						<input class="quicksearch multiple-quicksearch" name="quicksearch" id="quicksearch" multiple="">
					</div>
				<?php endif; ?>

				<?php if ( ! isset( $this->_options_2['fulltextsearch_or_quicksearch'] ) || isset( $this->_options_2['fulltextsearch_or_quicksearch'] ) && $this->_options_2['fulltextsearch_or_quicksearch'] === '0' ) : ?>
					<div class="col-sm-12 mb-1 mb-sm-3">
						<input type="text" class="fullTextSearch" name="fullTextSearch" id="fullTextSearch">
					</div>
				<?php endif; ?>

			</div>

			<div class="row">

				<div class="col-sm-4 mb-1 mb-sm-3 multiple-select" <?php echo $showCompanies; ?> >

				<input class="searchable-dropdown" type="search" id="company-filter" placeholder="Rechercher..." name="compnay" data-list="company-list" autocomplete="off" required />
	<label for="company-filter" data-icon="&#128269;"></label>
	<datalist id="company-list">
	</datalist>

					<select style="display:none" class="company multiple" multiple="multiple" name="company" id="company" data-contenttype="afdelinger"></select>
					<label class="selectDropDownLabel">
						<span style="display:none" class="placeholder-text"><?php _e( '- Select department -', 'biltorvet-dealer-tools' ); ?></span>
					</label>
				</div>

				<div class="col-sm-4 mb-1 mb-sm-3 multiple-select" <?php echo $showMakeModel; ?>>
				
				<input class="searchable-dropdown" type="search" id="makes-filter" placeholder="Rechercher..." name="make" data-list="makes-list" autocomplete="off" required />
	<label for="makes-filter" data-icon="&#128269;"></label>
	<datalist id="makes-list">
		
	</datalist>

				<select class="make multiple" multiple="multiple" name="make" id="make" data-contenttype="mærker"></select>
					<label class="selectDropDownLabel">
						<span class="placeholder-text"><?php _e( '- Select make -', 'biltorvet-dealer-tools' ); ?></span>
					</label>
				</div>

				<div class="col-sm-4 mb-1 mb-sm-3 multiple-select" <?php echo $showMakeModel; ?>>
				
				<input class="searchable-dropdown" type="search" id="models-filter" placeholder="Rechercher..." name="models" data-list="models-list" autocomplete="off" required />
	<label for="models-filter" data-icon="&#128269;"></label>
	<datalist id="models-list">
	</datalist>

				<select class="model multiple" multiple="multiple" name="model" id="model" data-contenttype="modeller"></select>
					<label class="selectDropDownLabel">
						<span class="placeholder-text"><?php _e( '- Select model -', 'biltorvet-dealer-tools' ); ?></span>
					</label>
				</div>

				<div class="col-sm-4 mb-1 mb-sm-3 multiple-select" <?php echo $showVehicleStates; ?>>
				
				
				<input class="searchable-dropdown" type="search" id="vehicleStates-filter" placeholder="Rechercher..." name="vehicleStates" data-list="vehicleStates-list" autocomplete="off" required />
	<label for="vehicleStates-filter" data-icon="&#128269;"></label>
	<datalist id="vehicleStates-list">
	</datalist>

				<select class="vehiclestate multiple" multiple="multiple" name="vehicleState" id="vehicleState" data-contenttype="stande"></select>
					<label class="selectDropDownLabel">
						<span class="placeholder-text"><?php _e( '- Select vehicle state -', 'biltorvet-dealer-tools' ); ?></span>
					</label>
				</div>

				<div class="col-sm-4 mb-1 mb-sm-3 multiple-select" <?php echo $showPriceTypes; ?>>
				
				<input class="searchable-dropdown" type="search" id="priceTypes-filter" placeholder="Rechercher..." name="priceTypes" data-list="priceTypes-list" autocomplete="off" required />
	<label for="priceTypes-filter" data-icon="&#128269;"></label>
	<datalist id="priceTypes-list">
	</datalist>

				<select class="pricetype multiple" multiple="multiple" name="priceType" id="priceType" data-contenttype="pristyper"></select>
					<label class="selectDropDownLabel">
						<span class="placeholder-text"><?php _e( '- Select price type -', 'biltorvet-dealer-tools' ); ?></span>
					</label>
				</div>

				<div class="col-sm-4 mb-1 mb-sm-3 multiple-select" <?php echo $showProductTypes; ?>>
			
				
				<input class="searchable-dropdown" type="search" id="productTypes-filter" placeholder="Rechercher..." name="productTypes" data-list="productTypes-list" autocomplete="off" required />
	<label for="productTypes-filter" data-icon="&#128269;"></label>
	<datalist id="productTypes-list">
	</datalist>
				<select class="producttype multiple" multiple="multiple" name="productType" id="productType" data-contenttype="køretøjstyper"></select>
					<label class="selectDropDownLabel">
						<span class="placeholder-text"><?php _e( '- Select vehicle type -', 'biltorvet-dealer-tools' ); ?></span>
					</label>
				</div>

				<div class="col-sm-4 mb-1 mb-sm-3 multiple-select" <?php echo $showBodyTypes; ?>>
				
				
				<input class="searchable-dropdown" type="search" id="bodytypes-filter" placeholder="Rechercher..." name="bodytypes" data-list="bodytypes-list" autocomplete="off" required />
	<label for="bodytypes-filter" data-icon="&#128269;"></label>
	<datalist id="bodytypes-list">
	</datalist>

				<select class="bodytype multiple" multiple="multiple" name="bodyType" id="bodyType" data-contenttype="karosserityper"></select>
					<label class="selectDropDownLabel">
						<span class="placeholder-text"><?php _e( '- Select body type -', 'biltorvet-dealer-tools' ); ?></span>
					</label>
				</div>

				<div class="col-sm-4 mb-1 mb-sm-3 multiple-select" <?php echo $showPropellants; ?>>
				
				
				<input class="searchable-dropdown" type="search" id="propellants-filter" placeholder="Rechercher..." name="compnay" data-list="propellants-list" autocomplete="off" required />
	<label for="propellants-filter" data-icon="&#128269;"></label>
	<datalist id="propellants-list">
	</datalist>

				<select class="propellant multiple" multiple="multiple" name="propellant" id="propellant" data-contenttype="drivmiddeltyper"></select>
					<label class="selectDropDownLabel">
						<span class="placeholder-text"><?php _e( '- Select propellant -', 'biltorvet-dealer-tools' ); ?></span>
					</label>
				</div>
				
				<div class="col-sm-4 mb-1 mb-sm-3" ">
			
	<input type="search" class="searchable-dropdown" id="gears-filter" placeholder="Rechercher..." name="brands" data-list="gears-list" autocomplete="off" required />
	<label for="gears-filter" data-icon="&#128269;"></label>
	<datalist id="gears-list">
	  <select  >
	  <option Selected value="">- Vælg Gears - </option>

<?php
$gears = $filterOptions['GearCount'];

sort( $gears );

foreach ( $gears as $gear ) {
	if ( $gear === null ) {
		echo '<option value="NULL">Ingen Gear</option>';
	} else {

		echo '<option value="' . strval( $gear ) . '">' . strval( $gear ) . '</option>';
	}
}
?>

	  </select>
	</datalist>
				<select style="display:none;" name="maxdue-gears" id="maxdue-gears">

	<?php
	$gears = $filterOptions['GearCount'];

	sort( $gears );

	foreach ( $gears as $gear ) {
		if ( $gear === null ) {
			echo '<option value="NULL">Ingen Gear</option>';
		} else {

			echo '<option value="' . strval( $gear ) . '">' . strval( $gear ) . '</option>';
		}
	}
	?>

	</select>
				</div>
				<div class="col-sm-4 mb-1 mb-sm-3" >

				<input class="searchable-dropdown" type="search" id="door-filter" placeholder="Rechercher..." name="brands" data-list="door-list" autocomplete="off" required />
	<label for="door-filter" data-icon="&#128269;"></label>
	<datalist id="door-list">
	  <select  >

	<?php

	sort( $filterOptions['DoorCount'] );
	foreach ( $filterOptions['DoorCount'] as $Door ) {
		echo '<option value="' . strval( $Door ) . '">' . strval( $Door ) . '</option>';
	}

	?>

	  </select>
	</datalist>

	<select style="display:none;" name="number-of-doors" id="number-of-doors">

	<option Selected value=""> - Vælg Antal døre - </option>
	
	<?php

	sort( $filterOptions['DoorCount'] );
	foreach ( $filterOptions['DoorCount'] as $Door ) {
		echo '<option value="' . strval( $Door ) . '">' . strval( $Door ) . '</option>';
	}

	?>

	</select>
			
			</div>

			<div class="row" style="min-width: 100%;" id="maxdue_sliders">
			</div>
			<style>
			</style>
<div class="row" style="min-width: 100%;" >
<div class="col-sm-4 mt-3 mt-sm-0 mb-3" style="margin-top:10px;" >
				<label for="car-registration" class="float-left">Bilregistreringsår</label>
				<span class="float-right"><span id="car-registration-min" class="bdtSliderMinVal">2000</span> - <span class="bdtSliderMaxVal" id="car-registration-max"><?php echo date( 'Y' ) + 1; ?></span></span>
<!--  <input style="display:none;"  id='car-registration' type="range" value="2000" min="2000" max=""/>-->
  <input type="text" class="js-range-slider" id='car-registration'  name="my_range" value=""
  data-hide-min-max = 'true'
  data-hide-from-to = 'true'
		data-skin="round"
		data-type="double"
		data-min="2000"
		data-max="<?php echo date( 'Y' ) + 1; ?>"
		data-grid="false"
	/>
	<?php echo date( 'Y' ) + 1; ?>
<input style="display:none;"  type="number" maxlength="4" value="2000" class="car-registration-from"/>
<input style="display:none;" type="number" maxlength="4" value="<?php echo date( 'Y' ) + 1; ?>" class="car-registration-to"/>
				</div>

				
				<div class="col-sm-4 mt-3 mt-sm-0 mb-3" style="margin-top:10px;" >
				<label for="car-mileage" class="float-left">Max bil kilometertal</label>
				<span class="float-right"><span id="car-mileage-min" class="bdtSliderMinVal">0</span> - <span class="bdtSliderMaxVal"  id="car-mileage-max">200</span>Km</span>
				<input type="text" class="js-range-slider" id='car-mileage'  name="my_range" value=""
  data-hide-min-max = 'true'
  data-hide-from-to = 'true'
		data-skin="round"
		data-type="double"
		data-min="0"
		data-max="200"
		data-grid="false"
	/>
	
<input style="display:none;"  type="number" maxlength="4" value="0" class="car-mileage-from"/>
<input style="display:none;" type="number" maxlength="4" value="200" class="car-mileage-to"/>
				</div>

</div>
<div class="row" style='min-width: 100%;' >
	
<div class="col-sm-4 mb-3">
					<div class="bdtSliderContainer">
						<label for="consumptionRange" class="float-left"><?php _e( 'Consumption', 'biltorvet-dealer-tools' ); ?></label>
						<span class="float-right"><span class="bdtSliderMinVal"></span> - <span class="bdtSliderMaxVal"></span>km/l</span>
						<span class="clearfix"></span>
						<input class="bdtSlider" id="consumptionRange" type="text" />
					</div>
				</div>
<div class="col-sm-4 mt-3 mt-sm-0 mb-3">
					<div class="bdtSliderContainer">
						<label for="priceRange" class="float-left"><?php _e( 'Price', 'biltorvet-dealer-tools' ); ?></label>
						<span class="float-right"><span class="bdtSliderMinVal"></span> - <span class="bdtSliderMaxVal"></span></span>
						<span class="clearfix"></span>
						<input class="bdtSlider" id="priceRange" type="text" />
					</div>
				</div>
				<div class="col-sm-4 text-center text-sm-right">
					<button type="button" data-labelpattern="<?php _e( 'Show %u vehicles', 'biltorvet-dealer-tools' ); ?>" class="et_pb_button search bdt_bgcolor"><?php printf( __( 'Show %u vehicles', 'biltorvet-dealer-tools' ), do_shortcode( '[bdt_vehicletotalcount]' ) ); ?></button>
					<button type="button" class="reset bdt_color mt-4 mt-sm-2"><?php _e( 'Reset', 'biltorvet-dealer-tools' ); ?></button>

				</div>
			</div>
				</span>
			<div class="lds-ring"><div></div><div></div><div></div><div></div></div>
		</div>
	</div>
	<style src='' ></style>
<script src="" ></script>
