<?php
/**
 * Theme functions and definitions
 *
 * @package MaxDue
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( get_option( 'stylesheet' ) == 'child-theme-for-max-due-main' ) {

	// Includes.
	include 'includes/get-car-data.php';
	include 'includes/md-schedule-event.php';
	include 'includes/rest-routes.php';
	include 'includes/max-due-vehicle-search.php';



	// Actions.
	add_action( 'init', 'md_schedule_event' );


	function hello_elementor_child_scripts_styles() {

		wp_enqueue_style(
			'hello-elementor-child-style',
			get_stylesheet_directory_uri() . '/style.css',
			array(
				'hello-elementor-theme-style',
			),
			'1'
		);

		wp_register_style(
			'ion_slider_css',
			'https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css',
			array(
				'hello-elementor-theme-style',
			),
			'1'
		);

		wp_register_style(
			'maxdue_main_css',
			get_theme_file_uri( 'assets/css/main.css' ),
			array(
				'hello-elementor-theme-style',
			),
			time()
		);

		wp_enqueue_style( 'ion_slider_css' );
		wp_enqueue_style( 'maxdue_main_css' );

		wp_register_script( 'maxdue_ion_js', 'https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js', array( 'jquery' ), time(), true );
		// wp_register_script( 'maxdue_jquery_ui_js', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js', array( 'jquery', 'bdt_script', 'maxdue_ion_js' ), time(), true );
		wp_register_script( 'maxdue_main_js', get_theme_file_uri( 'assets/js/main.js' ), array( 'jquery', 'jquery-ui-autocomplete', 'bdt_script', 'maxdue_ion_js' ), time(), true );

		wp_enqueue_script( 'jquery-ui' );

		// wp_enqueue_script( 'maxdue_jquery_ui_js' );
		wp_enqueue_script( 'maxdue_ion_js' );
		wp_enqueue_script( 'maxdue_main_js' );

	}
	add_action( 'wp_enqueue_scripts', 'hello_elementor_child_scripts_styles', 20 );
	// md_get_car_data();

	 $auto_it_data = get_option( 'autoit_Results' );



	// add_action( 'md_get_car_data_cron', 'md_get_car_data' );

	add_action( 'rest_api_init', 'maxdue_register_rest_route' );


}

