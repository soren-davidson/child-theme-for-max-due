<?php
/**
 * Gets the API car data.
 *
 * @return void
 */
function md_get_car_data() {

	$url = 'https://vehicle-api-dev.autoitweb.dk/v2/vehicle?a=0ba5cdd0-e29d-4b0c-b836-f9d873e42c4a';

	  $ch = curl_init( $url );

	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
	curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );

	$body = curl_exec( $ch );

	$body = json_decode( $body );

	if ( ! empty( $body ) ) {

		$all_vehicles = $body->result->vehicles;

		$filter_data = array();

		$filter_options = array(
			'Mileage'      => array(),
			'FirstRegYear' => array(),
			'GearCount'    => array(),
			'DoorCount'    => array(),
		);

		foreach ( $all_vehicles as $vehicle ) {

			$vehicle_detail_url = 'https://vehicle-api-dev.autoitweb.dk/v2/vehicle/detail/' . $vehicle->documentId . '?a=0ba5cdd0-e29d-4b0c-b836-f9d873e42c4a';

			   $vd_ch = curl_init( $vehicle_detail_url );

			   curl_setopt( $vd_ch, CURLOPT_RETURNTRANSFER, true );
			   curl_setopt( $vd_ch, CURLOPT_CONNECTTIMEOUT, 10 );
			   curl_setopt( $vd_ch, CURLOPT_TIMEOUT, 10 );

			   $response_body = curl_exec( $vd_ch );
			   $response_body = json_decode( $response_body );

			$vehicle_detail = $response_body->result->properties;

			foreach ( $vehicle_detail as $property ) {
				if ( $property->id == 'Mileage' || $property->id == 'FirstRegYear' || $property->id == 'GearCount' || $property->id == 'DoorCount' ) {
					if ( ! in_array( $property->value, $filter_options[ $property->id ] ) ) {
						$filter_options[ $property->id ][] = $property->value;
					}
					$filter_data[ $vehicle->documentId ][ $property->id ] = $property->value;
				}

				if ( $property->id == 'DoorCount' ) {
					continue;
				}
			}
		}

		 update_option( 'filter_options', $filter_options );
		 update_option( 'autoit_Results', $filter_data );
	}
}
