<?php
/**
 * Performs Vehicle Search based on our custom filters. Mimics the orignal function.
 *
 * @return void
 */

 use Biltorvet\Factory\VehicleFactory;
 use Biltorvet\Helper\DataHelper;
 use Biltorvet\Controller\PriceController;

function max_due_vehicle_search() {

	$options = new stdClass();

	$options->_options     = get_option( 'bdt_options' );
	$options->_options_2   = get_option( 'bdt_options_2' );
	$options->_options_3   = get_option( 'bdt_options_3' );
	$options->_options_4   = get_option( 'bdt_options_4' );
	$options->_options_5   = get_option( 'bdt_options_5' );
	$options->_options_6   = get_option( 'bdt_options_6' );
	$options->bdt_root_url = rtrim( get_permalink( $options->_options['vehiclesearch_page_id'] ), '/' );

	$biltorvetAPI = new BiltorvetAPI( $options->_options['api_key'] );

	$currentPage = 1;
	$limit       = intval( 300 );
	$start       = ( $currentPage - 1 ) * $limit;

	$filterObject = new BDTFilterObject();

	if ( isset( $_SESSION['bdt_filter'] ) && $_SESSION['bdt_filter'] !== '' ) {
		$filterObject = new BDTFilterObject( json_decode( $_SESSION['bdt_filter'], true ) );
	}

	if ( isset( $_POST['filter'] ) && $_POST['filter'] != null ) {
		$filterObject = new BDTFilterObject( sanitize_post( $_POST['filter'] ) );
	}

	try {

		$filterObject->Start = $start;
		$filterObject->Limit = $limit;

		 $filterObject = UpdateFilterObjectWithValuesFromPluginOptions( $filterObject );

		$vehicleFeed = $biltorvetAPI->GetVehicles( $filterObject );

		$vehicles = $vehicleFeed->vehicles;

		update_option( 'vehicle_feed', $vehicles );

		$main_data = $vehicles;

		$all_vehicles_properties = get_option( 'autoit_Results' );

		$filter_data = array();

	//	$test_Arr = [];
	
		$vehicles = array_filter(
			$vehicles,
			function( $data ) use ( $all_vehicles_properties ) {

				$first_registered_year      = $all_vehicles_properties[ $data->documentId ]['FirstRegYear'];
				$current_car_mileage        = $all_vehicles_properties[ $data->documentId ]['Mileage'] / 1000;
				$data_car_registration_year = $_POST['filter']['car_registration_year'];
				$data_car_mileage           = $_POST['filter']['car_mileage'];

				$registered_year = ( $first_registered_year >= intval( $data_car_registration_year[0] ) ) && ( $first_registered_year <= intval( $data_car_registration_year[1] ) ) || $first_registered_year == '';

				$mileage = ( $current_car_mileage >= intval( $data_car_mileage[0] ) ) && ( $current_car_mileage <= intval( $data_car_mileage[1] ) ) || $current_car_mileage == '';

			//	var_dump($current_car_mileage, $data_car_mileage, $mileage);

				if($_POST['filter']['maxdue_gears'][0] == "NULL")
				{
					$_POST['filter']['maxdue_gears'][0] = null;
//					$maxdue_gears    = $all_vehicles_properties[ $data->documentId ]['GearCount'] ==  '' ;
				}
				else
				{
					$maxdue_gears    = $all_vehicles_properties[ $data->documentId ]['GearCount'] ==  $_POST['filter']['maxdue_gears'][0] ;

				}
				$number_of_doors = $all_vehicles_properties[ $data->documentId ]['DoorCount'] ==  $_POST['filter']['number_of_doors'][0] ;

			//	var_dump(! empty( $_POST['filter']['maxdue_gears'] ) && ! empty( $_POST['filter']['number_of_doors'] ));
			
		//	var_dump([ 'gears' => [$_POST['filter']['maxdue_gears'], $all_vehicles_properties[ $data->documentId ]['GearCount']],'doors' => [ $_POST['filter']['number_of_doors'], $all_vehicles_properties[ $data->documentId ]['DoorCount']]]);

		//	var_dump($all_vehicles_properties[ $data->documentId ]['GearCount'] == $_POST['filter']['maxdue_gears'][0]);
		//	var_dump($all_vehicles_properties[ $data->documentId ]['DoorCount'] == $_POST['filter']['number_of_doors'][0]);

			$test_Arr[] =[$_POST['filter']['maxdue_gears'], $_POST['filter']['number_of_doors']];

		//	var_dump( $maxdue_gears );
//var_dump( $number_of_doors );

				if (  ! empty( $_POST['filter']['maxdue_gears'] ) && ! empty( $_POST['filter']['number_of_doors'] ) ) {
					return ( $registered_year && $mileage && $maxdue_gears && $number_of_doors );

				}

				if ( ! empty( $_POST['filter']['maxdue_gears'] ) ) {
					return ( $registered_year && $mileage && $maxdue_gears );

				}
				if ( ! empty( $_POST['filter']['number_of_doors'] ) ) {
					return ( $registered_year && $mileage && $maxdue_gears );

				}
				return ( $registered_year && $mileage );
			}
		);
		$vehicles = array_values( $vehicles );

		update_option('test_arr', $test_Arr);

		update_option( 'difference_0', $filter_data );

		$modified_data = $vehicles;

		update_option( 'difference_1', array( $main_data, $_POST['filter']['car_registration_year'] ) );
		update_option( 'difference_2', array( $modified_data, $_POST['filter']['car_mileage'] ) );

		$orderByValues = $biltorvetAPI->GetOrderByValues();

	} catch ( Exception $e ) {
		return $e->getMessage();
	}

	$amountOfPages = ceil( $vehicleFeed->totalResults / $limit );

	ob_start();
	?>

	<div id="vehicle_search_results" class="vehicle_search_results" data-totalResults="<?php echo $vehicleFeed->totalResults; ?>">
		<div class="row resultsTitle">
			<div class="col-md-6">
				<h4>
					<?php printf( __( 'Your search returned <span class="bdt_color">%d cars</span>', 'biltorvet-dealer-tools' ), count( $vehicles ) ); ?>
				</h4>
			</div>

			<div class="col-md-6 searchFilter">
				<div class="row">
					<div class="col">
						<select name="orderBy" id="select-orderby">
							<option value=""><?php _e( '- Order by -', 'biltorvet-dealer-tools' ); ?></option>
							<?php
							foreach ( $orderByValues as $orderBy ) :
								?>
								<option value="<?php echo $orderBy; ?>"
									<?php if ( isset( $filterObject->OrderBy ) && $filterObject->OrderBy === $orderBy ) : ?>
										selected="selected"
									<?php endif; ?>
								><?php echo _e( $orderBy, 'biltorvet-dealer-tools' ); ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="col">
						<select name="ascDesc" id="select-asc-desc">
							<option value="desc"<?php echo $filterObject->Ascending !== 'true' || isset( $options->_options_2['default_sorting_order'] ) && isset( $options->_options_2['default_sorting_order'] ) === 'Descending' ? ' selected="selected"' : ''; ?>><?php _e( 'Descending', 'biltorvet-dealer-tools' ); ?></option>
							<option value="asc"<?php echo $filterObject->Ascending === 'true' || isset( $options->_options_2['default_sorting_order'] ) && isset( $options->_options_2['default_sorting_order'] ) === 'Ascending' ? ' selected="selected"' : ''; ?>><?php _e( 'Ascending', 'biltorvet-dealer-tools' ); ?></option>
						</select>
					</div>
				</div>
			</div>
			<div class="lds-ring-paging d-done" style="display: none; opacity: 0;"><div></div><div></div><div></div><div></div></div>
			<div class="clearfix"></div>
		</div>
		<div class="results">
			<div id="vehicle-row" class="row vehicle-row">
				<?php

					// $bdt_root_url = rtrim(get_permalink($options->_options['vehiclesearch_page_id']),'/');

				foreach ( $vehicles as $oVehicle ) {
					$link = $options->bdt_root_url . '/' . $oVehicle->uri;

					// @TODO: Refactor.
					// For new we convert the old vehicle object to the new, so it works with the new templates
					// PLUGIN_ROOT refers to the v2 root.

					/** @var Vehicle $vehicle */
					$vehicle           = VehicleFactory::create( json_decode( json_encode( $oVehicle ), true ) );
					$vehicleProperties = DataHelper::getVehiclePropertiesAssoc( $vehicle->getProperties() );
					$priceController   = new PriceController( $vehicle );
					$basePage          = $options->bdt_root_url;
					require PLUGIN_ROOT . 'templates/partials/_vehicleCard.php';
				}
				?>
			</div>
				</div>
				<div class="paging">
					<?php
						$buttonCurrentPage = $currentPage;

						$newEnd = ( $currentPage ) * $limit;

					if ( $buttonCurrentPage < $amountOfPages ) {
						echo '<button class="paging-button et_pb_button bdt_bgcolor" id="paging-button" data-current-page="' . $buttonCurrentPage . '" data-amount-of-pages="' . $amountOfPages . '" data-end="' . $newEnd . '" data-limit="' . $limit . '">Indlæs flere...</button>';
					}
					?>
					<div class="lds-ring-paging d-done" style="display: none; opacity: 0;"><div></div><div></div><div></div><div></div></div>
				</div>
			</div>
	<?php
	$content = ob_get_contents();
	ob_end_clean();

	// Save filter in session
	if ( session_status() == PHP_SESSION_NONE ) {
		session_start();
	}

	$_SESSION['bdt_filter'] = json_encode( $filterObject );
	session_write_close();

	return $content;

	die;

}

function UpdateFilterObjectWithValuesFromPluginOptions( BDTFilterObject $filterObject ) {
	if ( $filterObject->OrderBy === null && isset( $option->_options_2['default_sorting_value'] ) ) {
		$filterObject->OrderBy = $option->_options_2['default_sorting_value'];
	}

	if ( $filterObject->Ascending === null && isset( $option->_options_2['default_sorting_order'] ) ) {
		$filterObject->Ascending = $option->_options_2['default_sorting_order'] === 'Ascending' ? 'true' : 'false';
	}

	$filterObject->HideSoldVehicles               = isset( $option->_options_2['hide_sold_vehicles'] ) && $option->_options_2['hide_sold_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideLeasingVehicles            = isset( $option->_options_2['hide_leasing_vehicles'] ) && $option->_options_2['hide_leasing_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideFlexLeasingVehicles        = isset( $option->_options_2['hide_flexleasing_vehicles'] ) && $option->_options_2['hide_flexleasing_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideWarehousesaleVehicles      = isset( $option->_options_2['hide_warehousesale_vehicles'] ) && $option->_options_2['hide_warehousesale_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideCarLiteDealerLabelVehicles = isset( $option->_options_2['hide_carlite_dealer_label_vehicles'] ) && $option->_options_2['hide_carlite_dealer_label_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideRentalVehicles             = isset( $option->_options_2['hide_rental_vehicles'] ) && $option->_options_2['hide_rental_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideUpcomingVehicles           = isset( $option->_options_2['hide_upcoming_vehicles'] ) && $option->_options_2['hide_upcoming_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideWholesaleVehicles          = isset( $option->_options_2['hide_wholesale_vehicles'] ) && $option->_options_2['hide_wholesale_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideTrailerVehicles            = isset( $option->_options_2['hide_trailer_vehicles'] ) && $option->_options_2['hide_trailer_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideCommissionVehicles         = isset( $option->_options_2['hide_commission_vehicles'] ) && $option->_options_2['hide_commission_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideExportVehicles             = isset( $option->_options_2['hide_export_vehicles'] ) && $option->_options_2['hide_export_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideByTypeCar                  = isset( $option->_options_2['hide_typecar_vehicles'] ) && $option->_options_2['hide_typecar_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideByTypeVan                  = isset( $option->_options_2['hide_typevan_vehicles'] ) && $option->_options_2['hide_typevan_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideByTypeMotorcycle           = isset( $option->_options_2['hide_typemotorcycle_vehicles'] ) && $option->_options_2['hide_typemotorcycle_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideByTypeTruck                = isset( $option->_options_2['hide_typetruck_vehicles'] ) && $option->_options_2['hide_typetruck_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideByTypeBus                  = isset( $option->_options_2['hide_typebus_vehicles'] ) && $option->_options_2['hide_typebus_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideBrandNewVehicles           = isset( $option->_options_2['hide_brandnew_vehicles'] ) && $option->_options_2['hide_brandnew_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideADVehicles                 = isset( $option->_options_2['hide_ad_vehicles'] ) && $option->_options_2['hide_ad_vehicles'] === 'on' ? 'true' : null;
	$filterObject->HideBIVehicles                 = isset( $option->_options_2['hide_bi_vehicles'] ) && $option->_options_2['hide_bi_vehicles'] === 'on' ? 'true' : null;

	if ( isset( $option->_options_2['bdt_pricetypes'] ) && $option->_options_2['bdt_pricetypes'] !== '-1' ) {
		$filterObject->PriceTypes = array( $option->_options_2['bdt_pricetypes'] );
	}

	return $filterObject;
}
