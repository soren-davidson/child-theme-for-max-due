<?php
/**
 * Registers our Custom Rest Route.
 */
function maxdue_register_rest_route() {

	register_rest_route(
		'max-due-child-theme/v1',
		'/vehiclesearch/search',
		array(
			'methods'             => 'POST',
			'callback'            => 'max_due_vehicle_search',
			'permission_callback' => '__return_true',
		)
	);

}
