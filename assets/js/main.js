(function ($) {

    $(document).ready(function () {
        var vehicleSearch = $(document).find('.bdt .vehicle_search');
        var sliderAlternativeNamespace = false;
        var startInterval = 3000;

        var frontpageSearch = document.getElementById("frontpage_vehicle_search");
        setTimeout(removelistener, startInterval);

        searchFilterOptionsXHR();

        function searchFilterOptionsXHR() {

            GetUserFilterSettings();


            // console.log('WORKING');
            $.ajax({
                url: ajax_config.restUrl + 'autoit-dealer-tools/v1/filteroptions',
                method: 'POST',
                dataType: 'json',
                data: {
                    'action': 'get_filter_options',
                    'filter': filter,
                },
                cache: false,
                success: function (response) {

                    var bodyTypes = response.bodyTypes;
                    var companies = response.companies;
                    var makes = response.makes;
                    var models = response.models;
                    var priceTypes = response.priceTypes;
                    var productTypes = response.productTypes;
                    var propellant = response.propellant;
                    var vehicleStates = response.vehicleStates;

                    if(frontpageSearch)
                    {
                        
                        frontpageSearchButton.setAttribute("data-labelpattern", "Vis " + response.totalResults + " resultater");
                        frontpageSearchButton.innerText = "Vis " + response.totalResults + " resultater";
                    }
                    else
                    {
                        vehicleSearch.find('.search').text(vehicleSearch.find('.search').data('labelpattern').replace('%u', response.totalResults));
                    }
            

                    var companies = '<select>';
                    for (var i in response.companies) {
                        companies += '<option value="' + response.companies[i].key + '">' + response.companies[i].value + '</option>';
                    }

                    // console.log(companies);

                    companies += '</select>';
                    $('#company-list').empty();
                    $('#company-list').append(companies);

                    var makes = '<select>';
                    for (var i in response.makes) {
                        makes += '<option value="' + response.makes[i].name + '">' + response.makes[i].name + '</option>';
                    }

                    // console.log(makes);

                    makes += '</select>';
                    $('#makes-list').empty();
                    $('#makes-list').append(makes)

                    var models = '<select>';
                    for (var i in response.models) {
                        models += '<option value="' + response.models[i].name + '">' + response.models[i].name + '</option>';
                    }

                    // console.log(models);

                    models += '</select>';
                    $('#models-list').empty();
                    $('#models-list').append(models)

                    var vehicleStates = '<select>';
                    for (var i in response.vehicleStates) {
                        vehicleStates += '<option value="' + response.vehicleStates[i].name + '">' + response.vehicleStates[i].name + '</option>';
                    }

                    // console.log(vehicleStates);

                    vehicleStates += '</select>';
                    $('#vehicleStates-list').empty();
                    $('#vehicleStates-list').append(vehicleStates)

                    var priceTypes = '<select>';
                    for (var i in response.priceTypes) {
                        priceTypes += '<option value="' + response.priceTypes[i].name + '">' + response.priceTypes[i].name + '</option>';
                    }

                    // console.log(priceTypes);

                    priceTypes += '</select>';
                    $('#priceTypes-list').empty();
                    $('#priceTypes-list').append(priceTypes)

                    var productTypes = '<select>';
                    for (var i in response.productTypes) {
                        productTypes += '<option value="' + response.productTypes[i].name + '">' + response.productTypes[i].name + '</option>';
                    }

                    // console.log(productTypes);

                    productTypes += '</select>';
                    $('#productTypes-list').empty();
                    $('#productTypes-list').append(productTypes)

                    var bodytypes = '<select>';
                    for (var i in response.bodyTypes) {
                        bodytypes += '<option value="' + response.bodyTypes[i].name + '">' + response.bodyTypes[i].name + '</option>';
                    }


                    bodytypes += '</select>';
                    // console.log(bodytypes);

                    $('#bodytypes-list').empty();
                    $('#bodytypes-list').append(bodytypes)

                    var propellants = '<select>';
                    for (var i in response.propellants) {
                        propellants += '<option value="' + response.propellants[i].name + '">' + response.propellants[i].name + '</option>';
                    }

                    // console.log(propellants);

                    propellants += '</select>';
                    $('#propellants-list').empty();
                    $('#propellants-list').append(propellants)




                    // console.log(response);

                    $('input[data-list]').each(function () {
                        var availableTags = $('#' + $(this).attr("data-list")).find('option').map(function () {

                            var object = { label: this.innerHTML + ' ', value: this.value + ' ' };

                            return object;
                        }).get();
                        // console.log(availableTags);
                        // console.log($($(this).attr("data-list")));
                        /*
                          $(this ).autocomplete({
                            source: availableTags
                          });
                        */

                        $(this).autocomplete({
                            source: availableTags,
                            change: function (event, ui) {
                                searchFilterOptionsXHR();
                            }
                        }).on('focus', function () {
                            $(this).autocomplete('search', ' ');
                        }).on('search', function () {
                            if ($(this).val() === '') {
                                $(this).autocomplete('search', ' ');
                            }
                        });
    
                    });
                    
                },

            });
        }
        $('.reset').on('click', function()
        {
            $('.searchable-dropdown').each(function(){
                this.value = '';
            });
    
            searchFilterOptionsXHR();
        })
    

    function removelistener() {
        $(document).off('click', '.bdt .search');
        $(document).off('change', '.searchFilter select');
        
        $(document).off('click', '.bdt .reset');
        
        $(document).off('slideStop', '.bdtSlider');
        var car_registration = jQuery('#car-registration')[0];
        var car_mileage = jQuery('#car-mileage')[0];

        $(document).on('click', '.bdt .search', function (e) {
            e.preventDefault();; maxdue_start_vehicle_search();
        })
        $(document).on('change', '.searchFilter select', function (e) { e.preventDefault();; OrderByAndAscDesc(); });




        $('#maxdue-gears')[0].disabled = false;


        $('#number-of-doors')[0].disabled = false;
    }

    function retrieve_filter_values(id) {


        if ($(id)[0].value != '' || $(id)[0].value.length !== 0) {
            return [$(id)[0].value.trim()];
        }
        else {
            return [];
        }



    }

    function GetUserFilterSettings() {

        if($.bootstrapSlider)
        {
            sliderAlternativeNamespace = true;
        }

        var prsC = {
            id: $(this).attr('id'),
            min: 0,
            max: 10000000,
            range: true,
            value: [0, 10000000],
            step: 1,
            tooltip: 'hide' // Bootstrap v4 tooltips not supported by this plugin.
        };
        var priceRangeSlider = $('#priceRange').slider(prsC);

        var crsC = {
            id: $(this).attr('id'),
            min: 0,
            max: 10,
            range: true,
            value: [0, 10],
            step: 1,
            tooltip: 'hide' // Bootstrap v4 tooltips not supported by this plugin.
        };

        var consumptionRangeSlider = $('#consumptionRange').slider(crsC);

        var vehicleSearchResults = $(document).find('.bdt .vehicle_search_results');
        var priceMin = priceRangeSlider.data('slider').getValue()[0];
        priceMin = priceMin === -1 ? null : priceMin;
        var priceMax = priceRangeSlider.data('slider').getValue()[1];
        priceMax = priceMax === -1 ? null : priceMax;
        var consumptionMin = consumptionRangeSlider.data('slider').getValue()[0];
        consumptionMin = consumptionMin === -1 ? null : consumptionMin;
        var consumptionMax = consumptionRangeSlider.data('slider').getValue()[1];
        consumptionMax = consumptionMax === -1 ? null : consumptionMax;

        var $car_registration_from = $(".car-registration-from")
        var $car_registration_to = $(".car-registration-to")

        var $car_mileage_from = $(".car-mileage-from")
        var $car_mileage_to = $(".car-mileage-to")


        filter = {
            CompanyIds: retrieve_filter_values('#company-filter'),
            FullTextSearch: vehicleSearch.find('input[name=fullTextSearch]').val() === '' ? null : [vehicleSearch.find('input[name=fullTextSearch]').val()],
            Propellants: retrieve_filter_values('#propellants-filter'),
            Makes: retrieve_filter_values('#makes-filter'),
            Models: retrieve_filter_values('#models-filter'),
            BodyTypes: retrieve_filter_values('#bodytypes-filter'),
            ProductTypes: retrieve_filter_values('#productTypes-filter'),
            VehicleStates: retrieve_filter_values('#vehicleStates-filter'),
            PriceTypes: retrieve_filter_values('#priceTypes-filter'),
            PriceMin: priceRangeSlider !== null ? (priceRangeSlider.data('slider').getAttribute('min') !== priceMin ? priceMin : null) : null,
            PriceMax: priceRangeSlider !== null ? (priceRangeSlider.data('slider').getAttribute('max') !== priceMax ? priceMax : null) : null,
            ConsumptionMin: consumptionRangeSlider !== null ? (consumptionRangeSlider.data('slider').getAttribute('min') !== consumptionMin ? consumptionMin : null) : null,
            ConsumptionMax: consumptionRangeSlider !== null ? (consumptionRangeSlider.data('slider').getAttribute('max') !== consumptionMax ? consumptionMax : null) : null,
            Start: null,
            Limit: null,
            OrderBy: vehicleSearchResults.find('select[name=orderBy]').val() === '' ? null : vehicleSearchResults.find('select[name=orderBy]').val(),
            Ascending: vehicleSearchResults.find('select[name=ascDesc]').val() === 'asc' ? true : false,
            number_of_doors: retrieve_filter_values('#door-filter'),
            maxdue_gears: retrieve_filter_values('#gears-filter'),
            car_registration_year: [$car_registration_from[0].value, $car_registration_to[0].value],
            car_mileage: [$car_mileage_from[0].value, $car_mileage_to[0].value]

        }

    }

    function maxdue_start_vehicle_search() {
        StartLoadingAnimation();

        // Fetch results, append to dom and then scrollTop
        $.when(maxdue_vehicle_search()).then(function () {
            $('html, body').animate({
                scrollTop: $('.vehicle-row').offset().top - 150
            }, 500);
        });
    }

    function maxdue_vehicle_search() {

        GetUserFilterSettings();

        return $.ajax({
            url: ajax_config.restUrl + 'max-due-child-theme/v1/vehiclesearch/search',
            method: 'POST',
            dataType: 'json',
            data: {
                'action': 'vehicle_search',
                'filter': filter
            },
            cache: false,
            success: function (response) {

                $('#bdt_vehicle_search_results').html(response);

            },
            complete: function () {
                StopLoadingAnimationPaging();
                StopLoadingAnimation();

                $('#maxdue-gears')[0].disabled = false;

                $('#number-of-doors')[0].disabled = false;

            }
        });
    }

    function OrderByAndAscDesc() {
        // Start loader / spinner?
        StartLoadingAnimationPaging();

        GetUserFilterSettings();

        filter.OrderBy = $('#select-orderby').val() === null ? null : $('#select-orderby').val();
        filter.Ascending = $('#select-asc-desc').val() === 'asc' ? true : false;

        $.ajax({
            url: ajax_config.restUrl + 'max-due-child-theme/v1/vehiclesearch/search',
            method: 'POST',
            dataType: 'json',
            data: {
                'action': 'vehicle_search',
                'filter': filter
            },
            cache: false,
            success: function (response) {

                $('#vehicle_search_results').html(response);
            },
            complete: function () {
                StopLoadingAnimationPaging();
            }
        });
    }



    var vehicleSearch = $(document).find('.bdt .vehicle_search');
    var loadingAnimation = vehicleSearch.find('.lds-ring');


    function StartLoadingAnimation() {


        if (loadingAnimation.hasClass('d-none')) {
            loadingAnimation.css('opacity', 0).css('display', 'block').removeClass('d-none');
        }
        loadingAnimation.animate({ opacity: 1 }, 1000);
    }

    function StartLoadingAnimationPaging() {

        var vehicleSearchResultsLoading = $(document).find('.bdt .vehicle_search_results');
        var loadingAnimationPaging = vehicleSearchResultsLoading.find('.lds-ring-paging');

        if (loadingAnimationPaging.hasClass('d-none')) {
            loadingAnimationPaging.css('opacity', 0).css('display', 'block').removeClass('d-none');
        }
        loadingAnimationPaging.animate({ opacity: 1 }, 1000);
    }


    function StopLoadingAnimationPaging() {
        var vehicleSearchResultsLoading = $(document).find('.bdt .vehicle_search_results');
        var loadingAnimationPaging = vehicleSearchResultsLoading.find('.lds-ring-paging');

        loadingAnimationPaging.animate({ opacity: 0 }, 200, function () { $(this).css('display', 'none').addClass('d-none'); });
    }
    function StopLoadingAnimation() {
        loadingAnimation.animate({ opacity: 0 }, 200, function () { $(this).css('display', 'none').addClass('d-none'); });
    }
    // Select 2
    function RetrieveSelect2Values(id) {
        if ($(id).hasClass("select2-hidden-accessible")) {

            const selectValues = [];

            $(id).select2('data').forEach(values => {

                let data = {
                    val: values.id,
                }

                selectValues.push(data.val);
            })

            // console.log(selectValues);

            return selectValues;
        }
    }
    var $car_registration_range = $("#car-registration"),
        $car_mileage_range = $("#car-mileage"),

        $car_registration_from = $(".car-registration-from"),
        $car_registration_to = $(".car-registration-to"),

        $car_mileage_from = $(".car-mileage-from"),
        $car_mileage_to = $(".car-mileage-to"),
        range,
        //min = $range.data('min'),
        //max = $range.data('max'),
        from,
        to;

    var updateValues = function () {

    };

    $car_registration_range.ionRangeSlider({
        onChange: function (data) {
            from = data.from;
            to = data.to;
            $car_registration_from.prop("value", from);
            $car_registration_to.prop("value", to);
            $('#car-registration-min')[0].innerHTML = from;
            $('#car-registration-max')[0].innerHTML = to;


            //    // console.log(from, to);

        }
    });


    $car_mileage_range.ionRangeSlider({
        onChange: function (data) {
            from = data.from;
            to = data.to;
            $car_mileage_from.prop("value", from);
            $car_mileage_to.prop("value", to);
            $('#car-mileage-min')[0].innerHTML = from;
            $('#car-mileage-max')[0].innerHTML = to;


            //  // console.log(from, to);

        }
    });
});


    // This demo uses jQuery UI Autocomplete
    // https://jqueryui.com/autocomplete

    // Cannot style datalist elements yet, so get
    // each option value and pass to jQuery UI Autocomplete

})(jQuery);

